library(curl)
library(readr)
library(dplyr)
library(stringr)
library(tidyr)
library(httr)

# This is a one-off variant of covid-data-getnewdata.R to fix a broken upload:
# three papers were not uploaded on 2020-08-15, so we'll upload them now as
# an extra after 2020-10-03a.

# The current date we are working with, as a string
datadate <- "2020-10-03"

# We call the output files 2020-10-03b.
currcsvfile <- "covid-2020-10-03a.csv"
csvfile <- str_c("covid-new-", datadate, "b", ".csv")
risfile <- str_c("covid-new-", datadate, "b", ".ris")

newdata <- read_csv(currcsvfile,
                     col_types = cols(
                       .default = col_character(),
                       date = col_date()
                     )) %>%
  filter(key %in% c("LSR54485", "LSR54532", "LSR54945"))

write_csv(newdata, csvfile, na = "")

# inspired by
# https://gist.github.com/RaphaelS1/4120c2bc6034182cd56e26ac9b7b1827
riscitation <- function(x) {
  ris <- "TY  - JOUR\r\n"
  ris <- str_c(ris, "TI  - ", x$title, "\r\n")
  auths <- str_split(x$authors, "; ")[[1]]
  ris <- str_c(ris, "AU  - ",
               str_c(auths, collapse = "\r\nAU  - "), "\r\n")
  if (! is.na(x$abstract)) {
    ris <- str_c(ris, "AB  - ", x$abstract, "\r\n")
  }
  if (! is.na(x$journal)) {
    ris <- str_c(ris, "JO  - ", x$journal, "\r\n")
  }
  if (! is.na(x$volume)) {
    ris <- str_c(ris, "VL  - ", x$volume, "\r\n")
  }
  if (! is.na(x$issue)) {
    ris <- str_c(ris, "IS  - ", x$issue, "\r\n")
  }
  if (! is.na(x$pages)) {
    pagerange <- str_split(x$pages, "-")[[1]]
    if (length(pagerange) == 1) {
      ris <- str_c(ris, "SP  - ", pagerange[1], "\r\n",
                   "EP  - ", pagerange[1], "\r\n")
    } else {
      ris <- str_c(ris, "SP  - ", pagerange[1], "\r\n",
                   "EP  - ", pagerange[2], "\r\n")
    }
  }
  if (! is.na(x$date)) {
    ris <- str_c(ris, "PY  - ", format(x$date, "%Y"), "\r\n")
  }
  if (! is.na(x$pubmed_id)) {
    ris <- str_c(ris, "ID  - ", x$pubmed_id, "\r\n")
  }
  if (! is.na(x$url)) {
    # LK doesn't work for Covidence; we'll try UR as well and see if that helps
    ris <- str_c(ris, "LK  - ", x$url, "\r\n")
    ris <- str_c(ris, "UR  - ", x$url, "\r\n")
  }
  if (! is.na(x$doi)) {
    ris <- str_c(ris, "DO  - ", x$doi, "\r\n")
  }
  ris <- str_c(ris, "C1  - ", x$key, "\r\n")
  ris <- str_c(ris, "ER  - \r\n")
  
  ris
}

fileConn = file(risfile, "w")

for (i in 1:nrow(newdata)) {
 cat(riscitation(newdata[i,]), file = fileConn,
       sep = "", append = TRUE)
}

close(fileConn)

