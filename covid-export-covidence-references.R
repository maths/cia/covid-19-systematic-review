library(readr)
library(dplyr)
library(tidyr)
library(stringr)

# Given a Covidence-produced CSV file of papers for full-text screening,
# extract the relevant lines from the CSV(s) of uploaded papers to
# assist with locating relevant PDFs.

# The current date we are working with, as a string
datadate <- "2020-08-14"

# This time there were three imports performed
newfile1 <- "covid-new-2020-08-13.csv"
newfile2 <- "covid-complete-new-2020-08-13.csv"
newfile3 <- str_c("covid-new-", datadate, ".csv")
selectfile <- "review_97979_select_csv_20200819031704.csv"
outputfile <- str_c("covid-fulltext-", datadate, ".csv")

# Unfortunately, Covidence trims the author lists, so when we
# construct our joint key, we will take at most the first 4 authors.
trimauthors <- function(authors) {
  str_replace(authors, "((?:[^;]*; ){3}[^;]*); .*", "\\1")
}

# key,title,authors,journal,issn,volume,issue,pages,[day,month,year|date],
# publisher,pmc_id,pubmed_id,url,abstract,doi
# Note that the CSV format changed on 14/08/2020.
# The jointkey is to allow us to match these up with the Covidence
# data.  Covidence truncates very long titles, so we take the first
# 50 characters only.
new1 <- read_csv(newfile1,
                 col_types = cols(
                   .default = col_character(),
                   day = col_integer(),
                   year = col_integer()
                 )) %>%
  mutate(date = NA)
new2 <- read_csv(newfile2,
                 col_types = cols(
                   .default = col_character(),
                   day = col_integer(),
                   year = col_integer()
                 )) %>%
  mutate(date = NA)
new3 <- read_csv(newfile3,
                 col_types = cols(
                   .default = col_character(),
                   date = col_date()
                 )) %>%
  mutate(day = NA, month = NA, year = NA)

newpapers <- bind_rows(new1, new2, new3)
newpapers <- newpapers %>%
  mutate(jointkey = str_c(trimauthors(authors), "++",
                          str_replace_na(str_sub(title, 1, 50)), "++",
                          str_replace_na(journal)))

# The Covidence file has the structure:
#  Title,Authors,Abstract,Published Year,Published Month,Journal,
#  Volume,Issue,Pages,Accession Number,DOI,Ref,
#  Covidence #,Study,Notes,Tags
# We only care about the final three fields, so we will
# keep just the jointkey and the last three fields
selected <- read_csv(selectfile,
                     col_types = cols(
                       .default = col_character(),
                       `Published Year` = col_integer()
                     )) %>%
  transmute(jointkey = str_c(trimauthors(Authors), "++",
                             str_replace_na(str_sub(Title, 1, 50)), "++",
                             str_replace_na(Journal)),
            covidence_id = `Covidence #`,
            covidence_study_name = Study,
            covidence_status = "Selected",
            covidence_notes = Notes,
            covidence_tags = Tags)

# anti_join(selected, newpapers, by = "jointkey") has 1 entry
# where there were two outhors with the same name (or a name
# was inadvertantly repeated); Covidence removes the duplicate.
# We fix this manually before doing a join.
selected[selected$covidence_id == "#1243", "jointkey"] <-
  newpapers[newpapers$key == "LSR41013", "jointkey"]

# We are only interested in the papers listed as "selected";
# we add the Covidence information to the papers tibble
selectedpapers <- right_join(newpapers, selected, by = "jointkey") %>%
  select(-jointkey)

# And we're done with the Covidence status:
write_csv(selectedpapers, outputfile, na = "")
